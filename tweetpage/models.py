from django.db import models


class savedTweet(models.Model):
    tweet = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True)
