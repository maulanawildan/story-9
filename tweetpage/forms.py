from django import forms
from tweetpage import models


class tweetForm(forms.Form):
    tweet = forms.CharField(
        label='Tweeting... ',
        required=True,
        widget=forms.Textarea(attrs={
            'class': 'form-control',
            'type': 'text',
            'placeholder': "Let's tweet here!"})
    )


class Meta:
    model = models.savedTweet
    fields = ('tweet')
